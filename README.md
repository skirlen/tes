# BKN - Dashboard Operasional (Prototype)

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>

## Requirements
- PHP >= 7.3
- NPM
- BCMath PHP Extension
- Ctype PHP Extension
- Fileinfo PHP Extension
- JSON PHP Extension
- Mbstring PHP Extension
- OpenSSL PHP Extension
- PDO PHP Extension
- Tokenizer PHP Extension
- XML PHP Extension

## Installation
- This package ships with a __.env.example__ file in the root of the project. You must rename this file to just __.env__ Change database setting in __.env__ file as your setting
- `Composer install`
- `npm install`
- `php artisan key:generate`
- `php artisan migrate`
- `php artisan db:seed`
- `composer dump-autoload`

## Mockup
[Mockup and UI Guideline](https://sites.google.com/view/siasn/ui-mock-up/dashboard-operasional-si-asn?authuser=0)

## HTML Source 
[Gitlab source](https://gitlab.com/andreass.bayu/template-dashboard-siasn)


## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
