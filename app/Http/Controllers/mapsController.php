<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

// panggil model helpdesk_summary
use App\Models\helpdesk_summary;
use App\Models\layanan_act_stat;


class mapsController extends Controller
{
    
    public function index_helpdesk()
    {

        // mengambil data helpdesk_summary
        $helpdesk_summary = helpdesk_summary::orderBy('created_at', 'desc')->first();

        // mengirim data helpdesk_summary ke view maps
        // return view('maps', ['helpdesk_summary' => $helpdesk_summary]);

        
        // mengambil data layanan_act_stat
        $layanan_act_stat = layanan_act_stat::orderBy('prioritas', 'asc')->get();

        // mengirim data layanan_act_stat ke view maps
        return view('maps', ['layanan_act_stat' => $layanan_act_stat],['helpdesk_summary' => $helpdesk_summary]);
        //return view('maps',['helpdesk_summary' => $helpdesk_summary]);


    } 
      
    

}


    