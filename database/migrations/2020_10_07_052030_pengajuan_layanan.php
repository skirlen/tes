<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class PengajuanLayanan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()

    {
        Schema::create('pengajuan_layanan', function (Blueprint $table) {
            $table->integer('id')->primary();
            $table->integer('kode_layanan');
            $table->string('nama_layanan');
            $table->integer('status_layanan');
            $table->string('nama_status_pengajuan');
            $table->date('tanggal_pengajuan');
            $table->integer('kode_instansi');
            $table->string('nama_instansi');
            $table->integer('kode_provinsi');
            $table->string('nama_provinsi');
            $table->integer('kode_kota');
            $table->string('nama_kota');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pengajuan_layanan');
    }
}
