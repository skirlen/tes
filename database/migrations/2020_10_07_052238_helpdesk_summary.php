<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class HelpdeskSummary extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('helpdesk_summary', function (Blueprint $table) {
            $table->integer('id')->primary();
            $table->integer('ticket_summary');
            $table->integer('chat_summary');
            $table->integer('phone_summary');
            $table->integer('mail_summary');
            $table->integer('ticket_finished');
            $table->integer('ticket_processed');
            $table->integer('avg_chat_reply');
            $table->integer('helpdesk_rating');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('helpdesk_summary');
    }
}
