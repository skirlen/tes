<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class StatusLayanan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('status_layanan', function (Blueprint $table) {
            $table->integer('id')->primary();
            $table->integer('kode_layanan');
            $table->char('nama_layanan');
            $table->integer('status_layanan');
            $table->char('nama_status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('status_layanan');
    }
}
