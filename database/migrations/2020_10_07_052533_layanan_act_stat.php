<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class LayananActStat extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('layanan_act_stat', function (Blueprint $table) {
            $table->integer('id')->primary();
            $table->integer('kode_layanan');
            $table->char('nama_layanan');
            $table->integer('status_active');
            $table->char('nama_status');
            $table->integer('prioritas');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('layanan_act_stat');
    }
}
