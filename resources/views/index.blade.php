@extends('layout/base')

@section('container')

        <!-- Page Content  -->
        <div id="content">

            <div class="form-row">
                <div class="col-9">
                    <h5>Statistik Layanan</h5>
    
                    <form class="mt-4">
                        <div class="form-row">
                          <div class="col">
                            <select class="custom-select mr-sm-2">
                                <option selected>Pilih jenis instansi</option>
                            </select>
                          </div>
                          <div class="col">
                            <select class="custom-select mr-sm-2">
                                <option selected>Pilih instansi</option>
                            </select>
                          </div>
                          <div class="col">
                            <select class="custom-select mr-sm-2">
                                <option selected>Pilih provinsi</option>
                            </select>
                          </div>
                          <div class="col">
                            <select class="custom-select mr-sm-2">
                                <option selected>Pilih kota</option>
                            </select>
                          </div>

                          <div class="col">
                                <button  type="button"  class="btn btn-block btn-success rounded-0">Apply</button>
                          </div>
                        </div>
                    </form>
                    
                    <div class="mt-4 row">
                        <div class="col-3">
                            <h5 class="bottom-liner">Tampilan Statistik</h5>
                        </div>
        
                        <div class="col-3">
                            <h5 class="bottom-liner-inactive"><a href="{{ url('/maps')}}" class="no-style">Tampilan Peta</a></h5>
                        </div>
                        
        
                        <div class="col">
                            <div class="row pull-right" style="margin-right: -70px;">
                                <div class="col-5">
                                    <input type="date" class="form-control form-control-sm">
                                </div>
                                <div class="col-2s"><span>to</span></div>
                                <div class="col-5">
                                    <input type="date" class="form-control form-control-sm">
                                </div>
                            
                            </div>
                            
                        </div>
                        
                    </div>
    
                </div>
            </div>
            
            
            <div class="row">
                <div class="col-9">

                    <div class="mt-3">
                        <div class="form-row">
                            <div class="col">
                                <div class="custom-card text-center">
                                    <span>Layanan Perencanaan Kepegawaian</span>
                                    <div class="row mt-2">
                                        <div class="col">
                                            <span class="head-number txt-pink">1453</span>
                                            <br>
                                            <small class="txt-pink">Pengajuan</small>
                                            
                                        </div>
                                        <div class="col">
                                            <span class="head-number txt-orange">1268</span>
                                            <br>
                                            <small class="txt-orange">Proses</small>
                                            
                                        </div>
                                        <div class="col">
                                            <span class="head-number txt-green">1125</span>
                                            <br>
                                            <small class="txt-green">Selesai</small>
                                            
                                        </div>
                                    </div>
                                    <div class="mt-2">
                                        <div class="form-row">
                                            <div class="col">
                                                <button type="button" class="btn btn-block btn-c-green rounded-0">Detail</button>
                                            </div>
                                            <div class="col">
                                                <button type="button" class="btn btn-block btn-c-white rounded-0">Alert</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
        
                            <div class="col">
                                <div class="custom-card text-center">
                                    <span>Layanan Penetapan <br>NIP</span>
                                    <div class="row mt-2">
                                        <div class="col">
                                            <span class="head-number txt-pink">1453</span>
                                            <br>
                                            <small class="txt-pink">Pengajuan</small>
                                            
                                        </div>
                                        <div class="col">
                                            <span class="head-number txt-orange">1268</span>
                                            <br>
                                            <small class="txt-orange">Proses</small>
                                            
                                        </div>
                                        <div class="col">
                                            <span class="head-number txt-green">1125</span>
                                            <br>
                                            <small class="txt-green">Selesai</small>
                                            
                                        </div>
                                    </div>
                                    <div class="mt-2">
                                        <div class="form-row">
                                            <div class="col">
                                                <button type="button" class="btn btn-block btn-c-green rounded-0">Detail</button>
                                            </div>
                                            <div class="col">
                                                <button type="button" class="btn btn-block btn-c-white rounded-0">Alert</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
        
                            <div class="col">
                                <div class="custom-card text-center">
                                    <span>Layanan Peremajaan <br>Data</span>
                                    <div class="row mt-2">
                                        <div class="col">
                                            <span class="head-number txt-pink">1453</span>
                                            <br>
                                            <small class="txt-pink">Pengajuan</small>
                                            
                                        </div>
                                        <div class="col">
                                            <span class="head-number txt-orange">1268</span>
                                            <br>
                                            <small class="txt-orange">Proses</small>
                                            
                                        </div>
                                        <div class="col">
                                            <span class="head-number txt-green">1125</span>
                                            <br>
                                            <small class="txt-green">Selesai</small>
                                            
                                        </div>
                                    </div>
                                    <div class="mt-2">
                                        <div class="form-row">
                                            <div class="col">
                                                <button type="button" class="btn btn-block btn-c-green rounded-0">Detail</button>
                                            </div>
                                            <div class="col">
                                                <button type="button" class="btn btn-block btn-c-white rounded-0">Alert</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
        
                            <div class="col">
                                <div class="custom-card text-center">
                                    <span>Layanan Kenaikan <br>Pangkat</span>
                                    <div class="row mt-2">
                                        <div class="col">
                                            <span class="head-number txt-pink">1453</span>
                                            <br>
                                            <small class="txt-pink">Pengajuan</small>
                                            
                                        </div>
                                        <div class="col">
                                            <span class="head-number txt-orange">1268</span>
                                            <br>
                                            <small class="txt-orange">Proses</small>
                                            
                                        </div>
                                        <div class="col">
                                            <span class="head-number txt-green">1125</span>
                                            <br>
                                            <small class="txt-green">Selesai</small>
                                            
                                        </div>
                                    </div>
                                    <div class="mt-2">
                                        <div class="form-row">
                                            <div class="col">
                                                <button type="button" class="btn btn-block btn-c-green rounded-0">Detail</button>
                                            </div>
                                            <div class="col">
                                                <button type="button" class="btn btn-block btn-c-white rounded-0">Alert</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
        
                            <div class="col">
                                <div class="custom-card text-center">
                                    <span>Layanan Pindah <br>Instansi</span>
                                    <div class="row mt-2">
                                        <div class="col">
                                            <span class="head-number txt-pink">1453</span>
                                            <br>
                                            <small class="txt-pink">Pengajuan</small>
                                            
                                        </div>
                                        <div class="col">
                                            <span class="head-number txt-orange">1268</span>
                                            <br>
                                            <small class="txt-orange">Proses</small>
                                            
                                        </div>
                                        <div class="col">
                                            <span class="head-number txt-green">1125</span>
                                            <br>
                                            <small class="txt-green">Selesai</small>
                                            
                                        </div>
                                    </div>
                                    <div class="mt-2">
                                        <div class="form-row">
                                            <div class="col">
                                                <button type="button" class="btn btn-block btn-c-green rounded-0">Detail</button>
                                            </div>
                                            <div class="col">
                                                <button type="button" class="btn btn-block btn-c-white rounded-0">Alert</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                    </div>
    
                    <div class="mt-3">
                        <div class="form-row">
                            <div class="col">
                                <div class="custom-card text-center">
                                    <span>Layanan Status <br>Kependudukan</span>
                                    <div class="row mt-2">
                                        <div class="col">
                                            <span class="head-number txt-pink">1453</span>
                                            <br>
                                            <small class="txt-pink">Pengajuan</small>
                                            
                                        </div>
                                        <div class="col">
                                            <span class="head-number txt-orange">1268</span>
                                            <br>
                                            <small class="txt-orange">Proses</small>
                                            
                                        </div>
                                        <div class="col">
                                            <span class="head-number txt-green">1125</span>
                                            <br>
                                            <small class="txt-green">Selesai</small>
                                            
                                        </div>
                                    </div>
                                    <div class="mt-2">
                                        <div class="form-row">
                                            <div class="col">
                                                <button type="button" class="btn btn-block btn-c-green rounded-0">Detail</button>
                                            </div>
                                            <div class="col">
                                                <button type="button" class="btn btn-block btn-c-white rounded-0">Alert</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
        
                            <div class="col">
                                <div class="custom-card text-center">
                                    <span>Layanan SIM <br>Pemberhentian</span>
                                    <div class="row mt-2">
                                        <div class="col">
                                            <span class="head-number txt-pink">1453</span>
                                            <br>
                                            <small class="txt-pink">Pengajuan</small>
                                            
                                        </div>
                                        <div class="col">
                                            <span class="head-number txt-orange">1268</span>
                                            <br>
                                            <small class="txt-orange">Proses</small>
                                            
                                        </div>
                                        <div class="col">
                                            <span class="head-number txt-green">1125</span>
                                            <br>
                                            <small class="txt-green">Selesai</small>
                                            
                                        </div>
                                    </div>
                                    <div class="mt-2">
                                        <div class="form-row">
                                            <div class="col">
                                                <button type="button" class="btn btn-block btn-c-green rounded-0">Detail</button>
                                            </div>
                                            <div class="col">
                                                <button type="button" class="btn btn-block btn-c-white rounded-0">Alert</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
        
                            <div class="col">
                                <div class="custom-card text-center">
                                    <span>Layanan SIM <br>Talent Manajemen</span>
                                    <div class="row mt-2">
                                        <div class="col">
                                            <span class="head-number txt-pink">1453</span>
                                            <br>
                                            <small class="txt-pink">Pengajuan</small>
                                            
                                        </div>
                                        <div class="col">
                                            <span class="head-number txt-orange">1268</span>
                                            <br>
                                            <small class="txt-orange">Proses</small>
                                            
                                        </div>
                                        <div class="col">
                                            <span class="head-number txt-green">1125</span>
                                            <br>
                                            <small class="txt-green">Selesai</small>
                                            
                                        </div>
                                    </div>
                                    <div class="mt-2">
                                        <div class="form-row">
                                            <div class="col">
                                                <button type="button" class="btn btn-block btn-c-green rounded-0">Detail</button>
                                            </div>
                                            <div class="col">
                                                <button type="button" class="btn btn-block btn-c-white rounded-0">Alert</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
        
                            <div class="col">
                                <div class="custom-card text-center">
                                    <span>Layanan <br>Wasdal</span>
                                    <div class="row mt-2">
                                        <div class="col">
                                            <span class="head-number txt-pink">1453</span>
                                            <br>
                                            <small class="txt-pink">Pengajuan</small>
                                            
                                        </div>
                                        <div class="col">
                                            <span class="head-number txt-orange">1268</span>
                                            <br>
                                            <small class="txt-orange">Proses</small>
                                            
                                        </div>
                                        <div class="col">
                                            <span class="head-number txt-green">1125</span>
                                            <br>
                                            <small class="txt-green">Selesai</small>
                                            
                                        </div>
                                    </div>
                                    <div class="mt-2">
                                        <div class="form-row">
                                            <div class="col">
                                                <button type="button" class="btn btn-block btn-c-green rounded-0">Detail</button>
                                            </div>
                                            <div class="col">
                                                <button type="button" class="btn btn-block btn-c-white rounded-0">Alert</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
        
                            <div class="col">
                                <div class="custom-card text-center">
                                    <span>Layanan <br>Bankum</span>
                                    <div class="row mt-2">
                                        <div class="col">
                                            <span class="head-number txt-pink">1453</span>
                                            <br>
                                            <small class="txt-pink">Pengajuan</small>
                                            
                                        </div>
                                        <div class="col">
                                            <span class="head-number txt-orange">1268</span>
                                            <br>
                                            <small class="txt-orange">Proses</small>
                                            
                                        </div>
                                        <div class="col">
                                            <span class="head-number txt-green">1125</span>
                                            <br>
                                            <small class="txt-green">Selesai</small>
                                            
                                        </div>
                                    </div>
                                    <div class="mt-2">
                                        <div class="form-row">
                                            <div class="col">
                                                <button type="button" class="btn btn-block btn-c-green rounded-0">Detail</button>
                                            </div>
                                            <div class="col">
                                                <button type="button" class="btn btn-block btn-c-white rounded-0">Alert</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                    </div>
    
                    <div class="mt-3">
                        <div class="form-row">
                            <div class="col-7">
                                <div class="custom-card">
                                    <figure class="highcharts-figure">
                                    <div id="container-chart"></div>
                                    </figure>
                                    <script>
                                    $(function(){
                                    Highcharts.chart('container-chart', {
                                    chart: {
                                        type: 'areaspline',
                                        backgroundColor: '#314268',
                                    },
                                    title: {
                                        text: 'Performance by month',
                                        style: {
                                            color: '#FFFFFF',
                                        }
                                    },
                                    legend: {
                                        enabled:false,
                                    },
                                    xAxis: {
                                        categories: [
                                        'Jan',
                                        'Feb',
                                        'Mar',
                                        'Apr',
                                        'May',
                                        'June',
                                        'July',
                                        'Aug', 
                                        'Sept',
                                        'Okt',
                                        'Nov',
                                        'Dec'
                                        ],
                                        labels:{
                                            style: {
                                                color: '#FFFFFF'
                                            }  
                                        }
                                    },
                                    yAxis: {
                                        title: {
                                        text: 'Value',
                                        style: {
                                            color: '#FFFFFF'
                                        }
                                        },
                                        labels: {
                                            style: {
                                                color: '#FFFFFF'
                                            }
                                        }
                                    },
                                    exporting: {
                                        enabled: false
                                    },
                                    tooltip: {
                                        shared: true,
                                        useHTML: true,
                                        backgroundColor: '#235695',
                                        headerFormat: '<table class="tooltip-table">',
                                        pointFormat: '<tr class="text-center"><td style="color:#F5DA7B"><h3 style="font-size:20px"><b>523.659</b><h3><p style="font-size:10px">Pengajuan</p></td>' +
                                            '<td style="color:#f0943d"><h3 style="font-size:20px"><b>459.589</b><h3><p style="font-size:10px">Diproses</p></td>'+
                                            '<td style="color:#63be5e"><h3 style="font-size:20px"><b>256.356</b><h3><p style="font-size:10px">Pengajuan Selesai</p></td>'+
                                            '<td style="color:#db4282"><h3 style="font-size:20px"><b>5.869</b><h3><p style="font-size:10px">Berkas Ditolak</p></td></tr>',
                                        footerFormat: '</table>',
                                        valueDecimals: 2
                                    },
                                    credits: {
                                        enabled: false
                                    },
                                    plotOptions: {
                                        areaspline: {
                                        fillOpacity: 0.5
                                        },
                                        series: {
                                            fillColor: {
                                            linearGradient: [0, 300, 0, 0],
                                             stops: [
                                                [0, Highcharts.getOptions().colors[0]],
                                                [1, Highcharts.color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
                                                ]
                                            }
                                        }
                                    },
                                    series: [{
                                        name: 'Pengajuan',
                                        data: [3, 4, 3, 5, 4, 10, 12,12,32,42,12,1]
                                    }]
                                    })
                                    });
                                    </script>
                                </div>
                            </div>
    
                            <div class="col">
                                <div class="custom-card">
                                    <figure class="highcharts-figure">
                                        <div id="container-pie"></div>
                                        
                                    </figure>
                                    <script>
                                    $(function(){
                                        Highcharts.setOptions({
                                            colors: ['#f0943d', '#D5352F', '#63be5e']
                                            });
                                        Highcharts.chart('container-pie', {
                                            chart: {
                                                backgroundColor: '#314268', 
                                                plotBackgroundColor: '#314268',
                                                plotBorderWidth: null,
                                                plotShadow: false,
                                                type: 'pie'
                                            },
                                            title: {
                                                text: 'Overview',
                                                style: {
                                                    fontSize: '15px',
                                                    color: '#FFFFFF'
                                                }
                                            },
                                            tooltip: {
                                                pointFormat: '{series.name}: <b>{point.y}</b>'
                                            },

                                            credits: {
                                                enabled: false
                                            },
                                            legend: {
                                                y: 20,
                                                itemStyle: {
                                                    color: '#FFFFFF',
                                                    fontWeight: 'bold'
                                                }
                                            },
                                            plotOptions: {
                                                pie: {
                                                    size:100,
                                                    borderWidth:0,
                                                    allowPointSelect: true,
                                                    cursor: 'pointer',
                                                    dataLabels: {
                                                        enabled: false
                                                    },
                                                    showInLegend: true
                                                }
                                            },
                                            exporting: {
                                                enabled: false
                                            },
                                            subtitle: {
                                                text: '3986<br>Pengajuan',
                                                style: {
                                                    fontSize: '12px',
                                                    color: '#FFFFFF',
                                                    fontWeight: 'bold'
                                            },
                                                align: 'center',
                                                verticalAlign: 'middle',
                                                y: 5
                                            },
                                            series: [{
                                                name: 'Result',
                                                innerSize: '80%',
                                                colorByPoint: true,
                                                data: [{
                                                    name: 'Diproses',
                                                    y: 61000
                                                }, {
                                                    name: 'Ditolak',
                                                    y: 11840
                                                }, {
                                                    name: 'Selesai',
                                                    y: 100900
                                                }]
                                            }]
                                        });
                                    })
                                    </script>
                                </div>
                            </div>
    
                            <div class="col">
                                <div class="custom-card">
                                    <figure class="highcharts-figure">
                                        <div id="container-p"></div>
                                    </figure>
                                    <script>
                                    $(function(){
                                        Highcharts.setOptions({
                                            colors: ['#63be5e', '#D5352F']
                                            });
                                        Highcharts.chart('container-p', {
                                            chart: {
                                                backgroundColor: '#314268',
                                                plotBackgroundColor: '#314268',
                                                plotBorderWidth: 0,
                                                plotShadow: false
                                            },
                                            title: {
                                                text: 'Task Complete',
                                                style: {
                                                    fontSize: '13px',
                                                    color: '#FFFFFF'
                                            }
                                            },
                                            subtitle: {
                                                text: '67%',
                                                style: {
                                                    fontSize: '35px',
                                                    color: '#FFFFFF',
                                                    fontWeight: 'bold'
                                            },
                                                align: 'center',
                                                verticalAlign: 'middle',
                                                y: 22
                                            },
                                            tooltip: {
                                                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                                            },
                                            accessibility: {
                                                point: {
                                                    valueSuffix: '%'
                                                }
                                            },
                                            legend: {
                                                y: 20,
                                                itemStyle: {
                                                    color: '#FFFFFF',
                                                    fontWeight: 'bold'
                                                }
                                            },                                            
                                            credits: {
                                                enabled: false
                                            },
                                            exporting: {
                                                enabled: false
                                            },
                                            
                                            plotOptions: {
                                                pie: {
                                                    dataLabels: {
                                                        enabled: false
                                                    },
                                                    startAngle: 0,
                                                    endAngle: 360,
                                                    borderWidth:0,
                                                    center:['50%','40%'],
                                                    size: 120,
                                                    showInLegend: true
                                                }
                                            },
                                            series: [{
                                                type: 'pie',
                                                name: 'Task Complete',
                                                innerSize: '90%',
                                                data: [
                                                    ['complete', 67],
                                                    ['incomplete', 33]
                                                ]
                                            }]
                                        });

                                    })
                                    </script>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
    
                <div class="col-3">
                    <div class="mt-3">
                        <div class="helpdesk-card text-center">
                            <span class="helpdesk-header">Helpdesk Alert</span>

                            <div class="mt-3">
                                <div class="form-row">
                                    <div class="col">
                                        <div class="box box-blue">
                                            <span class="head-number txt-orange">4.189.121</span>
                                            <br>
                                            <small>Ticket Summary</small>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="box box-blue">
                                            <span class="head-number txt-orange">1.189.121</span>
                                            <br>
                                            <small>Chat Summary</small>
                                        </div>
                                    </div>
                                    
                                </div>
                                
                            </div>

                            <div class="mt-2">
                                <div class="form-row">
                                    <div class="col">
                                        <div class="box box-blue">
                                            <span class="head-number txt-orange">989.121</span>
                                            <br>
                                            <small>Phone Summary</small>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="box box-blue">
                                            <span class="head-number txt-orange">2.189.121</span>
                                            <br>
                                            <small>Mail Summary</small>
                                        </div>
                                    </div>
                                    
                                </div>
                                
                            </div>

                            <div class="mt-2">
                                <div class="form-row">
                                    <div class="col">
                                        <div class="box box-blue">
                                            <span class="head-number txt-orange">3.189.121</span>
                                            <br>
                                            <small>Ticket Finished</small>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="box box-blue">
                                            <span class="head-number txt-orange">1.189.121</span>
                                            <br>
                                            <small>Ticket Processed</small>
                                        </div>
                                    </div>
                                    
                                </div>
                                
                            </div>

                            <div class="mt-2">
                                <div class="form-row">
                                    <div class="col">
                                        <div class="box box-blue">
                                            <span class="head-number txt-orange">5 Minute</span>
                                            <br>
                                            <small>Avg. Chat Reply</small>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="box box-blue">
                                            <span class="head-number txt-orange">90%</span>
                                            <br>
                                            <small>Helpdesk Rating</small>
                                        </div>
                                    </div>
                                    
                                </div>
                                
                            </div>

                            <div class="mt-3">
                                <span>Active Status</span>
                                <div class="form-row mt-2">
                                    <div class="col-2">
                                        <div class="txt-box">
                                            <small>#2462</small>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="txt-box">
                                            <small>Pelayanan SIASN</small>
                                        </div>
                                    </div>
                                    <div class="col-3">
                                        <div class="box-sm box-red">
                                            <small>URGENT</small>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-row mt-1">
                                    <div class="col-2">
                                        <div class="txt-box">
                                            <small>#2461</small>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="txt-box">
                                            <small>Pelayanan Bankum</small>
                                        </div>
                                    </div>
                                    <div class="col-3">
                                        <div class="box-sm box-pink">
                                            <small>High</small>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-row mt-1">
                                    <div class="col-2">
                                        <div class="txt-box">
                                            <small>#2460</small>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="txt-box">
                                            <small>PTSP</small>
                                        </div>
                                    </div>
                                    <div class="col-3">
                                        <div class="box-sm box-orange">
                                            <small>Medium</small>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-row mt-1">
                                    <div class="col-2">
                                        <div class="txt-box">
                                            <small>#2459</small>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="txt-box">
                                            <small>Permintaan Informasi Publik</small>
                                        </div>
                                    </div>
                                    <div class="col-3">
                                        <div class="box-sm box-teal">
                                            <small>Regular</small>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-row mt-1">
                                    <div class="col-2">
                                        <div class="txt-box">
                                            <small>#2458</small>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="txt-box">
                                            <small>Pelayanan SIASN</small>
                                        </div>
                                    </div>
                                    <div class="col-3">
                                        <div class="box-sm box-pink">
                                            <small>HIGH</small>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div class="mt-3">
                                <span>Status Layanan</span>
                                <div class="form-row mt-2">
                                    <div class="col">
                                        <div class="txt-box">
                                            <small>Layanan Perencanaan Kepegawaian</small>
                                            <br>
                                            <small class="txt-sm txt-orange">Sedang dalam maintenance</small>
                                        </div>
                                    </div>
                                    <div class="col-3">
                                        <div class="box-sm box-orange">
                                            <small>Detail</small>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-row mt-2">
                                    <div class="col">
                                        <div class="txt-box">
                                            <small>Layanan Bankum</small>
                                            <br>
                                            <small class="txt-sm txt-orange">Dilaporkan error</small>
                                        </div>
                                    </div>
                                    <div class="col-3">
                                        <div class="box-sm box-orange">
                                            <small>Detail</small>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-row mt-2">
                                    <div class="col">
                                        <div class="txt-box">
                                            <small>Layanan Pindah Instansi</small>
                                            <br>
                                            <small class="txt-sm txt-orange">Dilaporkan error</small>
                                        </div>
                                    </div>
                                    <div class="col-3">
                                        <div class="box-sm box-orange">
                                            <small>Detail</small>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-row mt-2">
                                    <div class="col">
                                        <div class="txt-box">
                                            <small>Layanan Wasdal</small>
                                            <br>
                                            <small class="txt-sm txt-orange">Proses upgrade sistem</small>
                                        </div>
                                    </div>
                                    <div class="col-3">
                                        <div class="box-sm box-orange">
                                            <small>Detail</small>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-row mt-2">
                                    <div class="col">
                                        <div class="txt-box">
                                            <small>Layanan Kenaikan Pangkat</small>
                                            <br>
                                            <small class="txt-sm txt-orange">Backup data</small>
                                        </div>
                                    </div>
                                    <div class="col-3">
                                        <div class="box-sm box-orange">
                                            <small>Detail</small>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
        
            
            
        </div>
    </div>
 
@endsection