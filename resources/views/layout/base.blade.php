<!DOCTYPE html>

<head>
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/plugin/bootstrap-4.5.2-dist/css/bootstrap.min.css') !!}">
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/custom.css') !!}">
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/plugin/fontawesome/font-awesome.min.css') !!}">
    <script src="{{ asset('assets/plugin/bootstrap-4.5.2-dist/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('assets/plugin/jquery/jquery-3.5.1.min.js') }}"></script>
    <script src="{{ asset('assets/plugin/highcharts_map/highmaps.js') }}"></script>
    <script src="{{ asset('assets/plugin/highcharts_map/modules/exporting.js') }}"></script>
    <script src="{{ asset('assets/plugin/highcharts_map/modules/export-data.js') }}"></script>
    <script src="{{ asset('assets/plugin/highcharts_map/modules/accessibility.js') }}"></script>
    <script src="{{ asset('assets/plugin/highcharts_map/highcharts.js') }}"></script>
    <!-- <script src="https://code.highcharts.com/mapdata/countries/id/id-all.js"></script> -->
    <script src="{{ asset('assets/plugin/highcharts_map/id-all.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
    
    <script type="text/javascript">
        $(document).ready(function () {
            $('#sidebarCollapse').on('click', function () {
                $('#sidebar').toggleClass('active');
            });
        });
    </script>

    <style>
        hr{
            border-top: 1px solid #fff;
        }
        .num-data{
            font-size: 1.3rem;
            font-weight: bold;
            color: #e8954f;
        }
        .side-box{
            background-color: #222a58;
            padding-top: 15px;
            padding-bottom: 15px;
        }
        .data-box{
            border: 3px solid #00a098;
            margin-bottom: 10px;
        }
        .title-box{
            margin-bottom: 15px;
        }
        .sub-title{
            font-size: 17px;
            margin: 5px;
        }
        .label-urgent{
            border: 2px solid #c33c38;
            text-align: center!important;
        }
        .label-high{
            border: 2px solid #db4282;
            text-align: center!important;
        }
        .label-medium{
            border: 2px solid #f0943d;
            text-align: center!important;
        }
        .label-regular{
            border: 2px solid #00a098;
            text-align: center!important;
        }
        .row-box{
            align-items: center;
            margin-bottom: 3px;
        }
        .info-layanan{
            color: #de985a;
        }
        .tooltip-table td{
            padding: 20px;
        }
        .tooltip-btn{
            width: 100%;
        }
        .tooltip-btn button{
            width: 100%;
        }
    </style>
</head>

<body>
    <nav class="navbar navbar-expand-lg navbar-dark bg-purple">
        <a class="navbar-brand" href="#">
            <img src="assets/images/logo_150x50.png" alt="">
        </a>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="#"><h5>Dashboard Operasional SI-ASN Badan Kepegawaian Negara</h5></a>
            </li>
            </ul>
            <div class="avatar-rectangle">
                <span class="initials">EW</span>
            </div>
            <ul class="navbar-nav my-2">
                <li class="nav-item active">
                      <a class="nav-link" href="#">Evans Winanda <i class="fa fa-angle-down"></i></a>
                </li>
            </ul>
        </div>
    </nav>

    <div class="wrapper">
        <!-- Sidebar  -->
        <nav id="sidebar" class="active">
            <ul class="list-unstyled components">
                <li class="active">
                    <a href="#">
                        <i class="far fa-square"></i>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <i class="fas fa-image"></i>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <i class="fas fa-image"></i>
                    </a>
                </li>
                <li>
                    
                </li>
            </ul>
            <button type="button" id="sidebarCollapse" class="btn btn-c-green rounded-circle bg-dark" style="bottom: 0; position: fixed; left: 10px;">
                <i class="fa fa-bars"></i>
                <span></span>
            </button>
        </nav>

@yield('container')

</body>