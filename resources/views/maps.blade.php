@extends('layout/base')

@section('container')
        <!-- Page Content  -->
        <div id="content">
            <div class="form-row">
                <div class="col-9">
                    <div class="mt-4 row">
                        <div class="col-3">
                            <h5 class="bottom-liner-inactive"><a href="{{ url('/')}}" class="no-style">Tampilan Statistik</a></h5>
                        </div>
        
                        <div class="col-3">
                            <h5 class="bottom-liner">Tampilan Peta</h5>
                        </div>
                        
        
                        <div class="col">
                            <div class="row pull-right" style="margin-right: -70px;">
                                <div class="col-5">
                                    <input type="date" class="form-control form-control-sm">
                                </div>
                                <div class="col-2s"><span>to</span></div>
                                <div class="col-5">
                                    <input type="date" class="form-control form-control-sm">
                                </div>
                            
                            </div>
                            
                        </div>
                        
                    </div>
    
                </div>
            </div>

            
            <div class="row mt-3">
                <div class="col-9">
                    <div id="container"></div>
                </div>
                <div class="col-3 side-box">
                    <h4 class="text-center title-box">Helpdesk Alert</h4>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="data-box text-center">
                                <div class="num-data">{{ $helpdesk_summary['ticket_summary'] }}</div>
                                <div>Ticket Summary</div>
                            </div>
                            <div class="data-box text-center">
                                <div class="num-data">{{ $helpdesk_summary['phone_summary'] }}</div>
                                <div>Phone Summary</div>
                            </div>
                            <div class="data-box text-center">
                                <div class="num-data">{{ $helpdesk_summary['ticket_finished'] }}</div>
                                <div>Ticket Finished</div>
                            </div>
                            <div class="data-box text-center">
                                <div class="num-data">{{ $helpdesk_summary['avg_chat_reply'] }}</div>
                                <div>Avg. Chat Reply</div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="data-box text-center">
                                <div class="num-data">{{ $helpdesk_summary['chat_summary'] }}</div>
                                <div>Chat Summary</div>
                            </div>
                            <div class="data-box text-center">
                                <div class="num-data">{{ $helpdesk_summary['mail_summary'] }}</div>
                                <div>Mail Summary</div>
                            </div>
                            <div class="data-box text-center">
                                <div class="num-data">{{ $helpdesk_summary['ticket_processed'] }}</div>
                                <div>Ticket Processed</div>
                            </div>
                            <div class="data-box text-center">
                                <div class="num-data">{{ $helpdesk_summary['helpdesk_rating'] }}</div>
                                <div>Helpdesk Rating</div>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="sub-title text-center">Active Status</div>
                
                    <div class="row row-box">
                    @foreach($layanan_act_stat as $layanan)   
                        <div class="col-2">
                           <div>{{ $layanan['kode_layanan'] }}</div> 
                        </div>
                        <div class="col-6">
                            <div>{{ $layanan['nama_layanan'] }}</div> 
                        </div>
                        <div class="col-4">
                            <div class="label-urgent">{{ $layanan['nama_status'] }}</div>
                        </div>
                    @endforeach
                    </div>

                    <hr>
                    <div class="sub-title text-center">Status Layanan</div>
                    <div class="row row-box">
                        <div class="col-9">
                            <div>Layanan Perencanaan Kepegawaian</div> 
                            <small class="info-layanan">sedang dalam maintenance</small>
                        </div>
                        <div class="col-3">
                            <button class="btn btn-outline-warning btn-sm">Detail</button>
                        </div>
                    </div>
                    <div class="row row-box">
                        <div class="col-9">
                            <div>Layanan BANKUM</div> 
                            <small class="info-layanan">dilaporkan error</small>
                        </div>
                        <div class="col-3">
                            <button class="btn btn-outline-warning btn-sm">Detail</button>
                        </div>
                    </div>
                    <div class="row row-box">
                        <div class="col-9">
                            <div>Layanan Pindah Instansi</div> 
                            <small class="info-layanan">dilaporkan error</small>
                        </div>
                        <div class="col-3">
                            <button class="btn btn-outline-warning btn-sm">Detail</button>
                        </div>
                    </div>
                    <div class="row row-box">
                        <div class="col-9">
                            <div>Layanan WASDAL</div> 
                            <small class="info-layanan">proses update sistem</small>
                        </div>
                        <div class="col-3">
                            <button class="btn btn-outline-warning btn-sm">Detail</button>
                        </div>
                    </div>
                    <div class="row row-box">
                        <div class="col-9">
                            <div>Layanan Kenaikan Pangkat</div> 
                            <small class="info-layanan">backup data</small>
                        </div>
                        <div class="col-3">
                            <button class="btn btn-outline-warning btn-sm">Detail</button>
                        </div>
                    </div>
                </div>
            </div>
            
            
            
        </div>
    </div>

    <script>
        $(function(){
            var data = [
                ['id-3700', 0],
                ['id-ac', 1],
                ['id-jt', 2],
                ['id-be', 3],
                ['id-bt', 4],
                ['id-kb', 5],
                ['id-bb', 6],
                ['id-ba', 7],
                ['id-ji', 8],
                ['id-ks', 9],
                ['id-nt', 10],
                ['id-se', 11],
                ['id-kr', 12],
                ['id-ib', 13],
                ['id-su', 14],
                ['id-ri', 15],
                ['id-sw', 16],
                ['id-ku', 17],
                ['id-la', 18],
                ['id-sb', 19],
                ['id-ma', 20],
                ['id-nb', 21],
                ['id-sg', 22],
                ['id-st', 23],
                ['id-pa', 24],
                ['id-jr', 25],
                ['id-ki', 26],
                ['id-1024', 27],
                ['id-jk', 28],
                ['id-go', 29],
                ['id-yo', 30],
                ['id-sl', 31],
                ['id-sr', 32],
                ['id-ja', 33],
                ['id-kt', 34]
            ];

            Highcharts.mapChart('container', { 
                chart: {
                    map: 'countries/id/id-all',
                    backgroundColor: '#324168',
                    height: 700
                },

                title: {
                    text: ''
                },

                subtitle: {
                    text: ''
                },

                mapNavigation: {
                    enabled: true,
                    buttonOptions: {
                        verticalAlign: 'bottom'
                    }
                },
                exporting: {
                enabled: false
                },
                colorAxis: {
                    min: 0
                },

                series: [{
                    data: data,
                    name: 'Layanan Penetapan NIP',
                    states: {
                        hover: {
                            color: '#BADA55'
                        }
                    },
                    dataLabels: {
                        enabled: true,
                        format: '{point.name}'
                    },
                    point: {
                events: {
                    click: function() {
                        this.series.chart.update({
                            tooltip: {
                                enabled: true,
                            }
                        });
                    },
                    mouseOut: function() {
                        this.series.chart.update({
                            tooltip: {
                                enabled: false
                            }

                        })
                    }
                }
            },
                }],
                tooltip: {
                    enabled:false,
                    crosshairs: [true,true],
                    followPointer: false,
                    useHTML: true,
                    backgroundColor: '#235695',
                    headerFormat: '<h4 class="text-center" style="color:white">{point.key}</h4><table class="tooltip-table">',
                    pointFormat: '<tr class="text-center"><td style="color:#db4282"><h3><b>1454</b><h3><p style="font-size:15px">Pengajuan</p></td>' +
                        '<td style="color:#f0943d;"><h3><b>1454</b><h3><p style="font-size:15px">Pengajuan</p></td>'+
                        '<td style="color:#63be5e"><h3><b>1454</b><h3><p style="font-size:15px">Pengajuan</p></td></tr></table>'+
                            '<table class="tooltip-btn"><tr class="text-center" style="width:100%"><td><button class="btn btn-primary">Detail</button></td>'+
                            '<td><button class="btn btn-light">Alert</button></td></tr>',
                    footerFormat: '</table>',
                    valueDecimals: 2
                },
            });
        })
    </script>

@endsection