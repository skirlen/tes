<?php

use App\Http\Controllers\Backend\Home\HomeController;

Route::get('/', [HomeController::class, 'index']);
