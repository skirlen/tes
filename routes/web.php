<?php

use Illuminate\Support\Facades\Route;

/**
 * Global Routes
 * Routes that are used between both frontend and backend.
 */

/*
 * Backend Routes
 * Namespaces indicate folder structure
 */


Route::group(['namespace' => 'Backend'], function () {
    /*
     * These routes need view-backend permission
     * (good if you want to allow more than one group in the backend,
     * then limit the backend features by different roles or permissions)
     *
     * Note: Administrator has all permissions so you do not have to specify the administrator role everywhere.
     */
    includeRouteFiles(__DIR__.'/Backend/');
});

Route::get('/', function () {
    return view('index');
});

use App\Http\Controllers\mapsController;

Route::get('/maps', [mapsController::class, 'index_helpdesk']);




